import { defineNuxtConfig } from 'nuxt'

// https://v3.nuxtjs.org/api/configuration/nuxt.config
export default defineNuxtConfig({
  meta: {
    charset: 'utf-8',
    viewport: 'width=device-width, initial-scale=1, maximum-scale=1',
    link: [
      { rel: 'icon', href: '/favicon.svg' }
    ]
  },
  css: ['@/assets/styles/main.scss'],
  runtimeConfig: {
    public: {
      apiBase: 'http://localhost:8808'
    }
  }
})
