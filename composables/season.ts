export const useSeason = async () => {
  const runtimeConfig = useRuntimeConfig()

  const { data: list } = await useFetch(`${runtimeConfig.public.apiBase}/api/bus/competition/v1/seasons/dictionaries`)
  const current = computed(() => list.value ? list.value.find(s => s.isCurrent) : null)

  return { current, list }
}
