import { defineNuxtPlugin } from '#app'

export default defineNuxtPlugin((nuxtApp) => {
  let scroll = null

  nuxtApp.hook('page:finish', () => {
    if (scroll) {
      scroll()
    }
  })

  nuxtApp.$router.options.scrollBehavior = (to, from, savedPosition) => {
    scroll = null

    if (to.hash) {
      return {
        el: to.hash,
        behavior: 'smooth'
      }
    }

    if (to.href === from.href) {
      return
    }

    return new Promise((resolve) => {
      scroll = () => {
        scroll = null
        resolve(savedPosition || { top: 0 })
      }
    })
  }
})
