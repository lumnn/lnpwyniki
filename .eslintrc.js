module.exports = {
  env: {
    node: true
  },
  extends: [
    '@nuxtjs/eslint-config-typescript',
    'eslint:recommended',
    'plugin:nuxt/recommended',
    'plugin:vue/vue3-recommended'
  ],
  rules: {
    // override/add rules settings here, such as:
    // 'vue/no-unused-vars': 'error'
  },
  // https://eslint.vuejs.org/user-guide/#using-eslint-v8-x
  parserOptions: {
    ecmaVersion: 'latest',
    sourceType: 'module'
  }
}
