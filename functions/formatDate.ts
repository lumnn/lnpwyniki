const dateFormatter = Intl.DateTimeFormat('pl-PL', { dateStyle: 'medium', timeStyle: 'short' })
const formatDate = function (date) {
  if (!date) {
    return ''
  }

  date = new Date(date)
  return dateFormatter.format(date)
}

export { formatDate }
